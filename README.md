## TODO

* use learn tailwind
  * https://www.tailwindcss.cn/
* control inventation error
* add appreance plugin
* the plugins cp less
* learn iconfont
* todo: use devtools to debug ios error
* fix the .wine to open presentation.html always
* how add update time , like tw
* [x] change fonts to friacode to support code equal symbol
* [x] add fontawesome use npm or local use css
* [x] use source font face(node_modules or url, not use absolute path)
* [x] resolve font face(best style)
* config reveal scrollbar
* how to add border like jyywiki seem ppt
* portbug: 35729 netstat -anp |grep 端口 conflict v2ray and v2raya(maybe)
* fix: reveal-menu path wrong in web, local is correct
* fix: chalkboard missing location bug
* ltodo: change reveal.vercel.app and git repo to presentions
* [x] change favicon to reveal.png
* [x] change favicon.ico(may be bug add issue for repo)
* pwalize
  https://github.com/webpro/reveal-md/pull/405/files
<!--* how embed in markdown-->
* [x] how use multi md to one html
* [x] upgrade reveal-md
* [x] custom output dir
* [x] config sepreator
* [x] resolve preprocessor confict newline for title
* config plugin
* [x] fix theme font
* [x] how to archive html like jyywiki.cn
* how to close port like vim-markdownpreview
* add custom css
* [ ] npm i -g reveal-md warngin no support
* [x] how to add extra css file
* [x] local how to change favicon
* add step by step
* [x] how to add linkstyle to html head like tw5
* config chalkboard
* add footer
* better multi index html, maybe can use template

## ref

* [article](https://palmer.arkstack.cn/2017/05/RevealJs-slides%E6%BC%94%E7%A4%BA%E5%B7%A5%E5%85%B7-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/)

## link

* [site](https://revealdoc.vercel.app)

## bug

* livereload have wrong(reboot??)
* use ctrl s html open, this *.exe will start in background

## shortcuts

* ?
* esc or o(overview)
> des:

* b(block)
> des:

* hijk
* whitespace
* ctrl and click (linux), win's command is alt
* shift -> or <-

## doc

* https://revealjs.com/

## tips

url ?print-pdf and use ctrl p

## tools

* inliner

## relatearticles

* https://blog.hanklu.tw/post/2021/use-reveal-md-to-generate-multiple-slides-and-host-them-on-github-page/


## hightlight

/usr/lib/node_modules/reveal-md/node_modules/highlight.js/styles/*
