---
title: test
---

# Test

~~dmeo~~

<div style="text-align: right;">
<p> dmeo </p>
<img src="images/favicon.png" title="qr" alt="qr" style="zoom: 40%;" />
</div>

> demo dmeo Lorem

> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
<!--s-->
## chapter 01

<!--v-->
### test 01

* dmeo
* demo
* demolo Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

> demokj

<!--v-->
### 确定主题

* 其中C是SHA256的压缩函数，+ 是mod  加法，即将两个数字加在一起，如果对取余, 是消息区块的哈希值.

SHA256的压缩函数主要对512位的消息区块和256位的中间哈希值进行操作，本质上，它是一个通过将消息区块为密钥对中间哈希值进行加密的256位加密算法。 因此，为了描述SHA256算法，有以下两方面的组件需要描述：

SHA256压缩函数
SHA256消息处理流程
1、公式




运算符号

符号	含义
∧	按位“与”
¬	按位“补”
⊕	按位“异或”
	循环右移n个bit
	右移n个bit


2、常量及初始化
SHA256算法中用到了8个哈希初值以及64个哈希常量

取自自然数中前面8个素数(2,3,5,7,11,13,17,19)的平方根的小数部分, 并且取前面的32位. 下面举个例子:

小数部分约为0.414213562373095048, 而其中

                                                                                         0.414213562373095048≈6∗16−1+a∗16−2+0∗16−3+...

于是，质数2的平方根的小数部分取前32bit就对应出了0x6a09e667

如此类推, 初始哈希值由以下8个32位的哈希初值构成:


* dmeo
* demo
* demolo Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

> demokj

<!--v-->
### code

```c
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
printf("This is a test.")
```
