---
title: Presentation
author: oeyoews
date: 2022-04-24T09:37:09
---


<div style="text-align: center;display: inline;">
<img src="images/favicon.png" style="zoom: 40%;" />
</div>
<!--s-->

<!--
# <i class="fa-solid  fa-fade fa-hourglass-start"></i> 开始
-->

<!--v-->
# SHA256 安全性
<!-- .slide: data-background="images/favicon.png" -->

<!--s-->
<!-- .slide: data-background="#ff0000" -->
## <i class="fa-solid fa-shake fa-user-friends"></i> 01 介绍

<!-- .slide: data-transition="concave" -->
<!--v-->
* 我们几乎每天都要使用密码， 密码的安全性也是我们很关心的一个问题，自密码产生以来， 对密码的破解工作一直都在进行着， 我们可能听说过的最多的关键词就是王小云(MD5及SHA-1破解者,~~国密算法SM3发明者??~~)。

<!--v-->
 密码破解往往需要花费很大的成本， 比如被王小云"破解"的MD5和SHA1其实是对碰撞攻击的破解,即找到了快速碰撞的方法，这是个巨大的进步，对数学和加密学都有很大的价值，但距离实用价值还有很大距离. 所以原像或者次原像攻击并没有受到很大的影响，也正是如此MD5在今天仍旧用作校验数据的完整性。

<!--v-->
>  在 SHA-1(1995) 首次发布的20年后，Google 宣布了首次可操作的生成冲突的技术。这是 Google 与 CWI(Centrum Wiskunde & Informatica ) Institute 合作 2 年的研究成果。为了证明他们确实掌握了攻击的方法，他们发布了2个 PDF 文件，两个文件的内容是不同的，但经过 SHA-1 算法哈希后却能得到相同的信息摘要。

<!--https://cloud.tencent.com/developer/article/1083026-->
<!--v-->
![img](images/samehash.png)

<!--v-->
![img](images/shattered.io.png)

<!--v-->
* 这种攻击需要超过9,223,372,036,854,775,808个SHA1计算。这使得等价的处理能力为6,500年的单CPU计算和110年的单GPU计算。
> ps: 不谈成本的攻击都是耍流氓

<!--
## <i class="fa-solid fa-beat fa-circle-question"></i> 02
-->

<!---->
* sha-2是sha-1算法标准的继承者， sha256就是其中的一类， 在sha-2 的公布出来的前期， 它的安全性颇受争议， 但是经过人们的努力和时间的证明， sha-2在今天看来还是比较成功的。那么sha-2到底有多安全呢？ 我们可能还没有一个比较直观的认识。

<!---->
* 破解sha256最常见的办法就是碰撞攻击，这里以sha256为例， sha256的算法还是很复杂的, 这里就大致介绍一下(~~可以不用记~~)


<!--https://www.cnblogs.com/hjs-junyu/p/10608986.html#:~:text=SHA256%E7%AE%97%E6%B3%95%E7%9A%84%E5%8E%9F%E7%90%86%E6%98%AF%EF%BC%9A%E7%94%A8%E8%BE%93%E5%85%A5%E7%9A%84%E9%95%BF%E5%BA%A6%E5%AF%B9448%E5%8F%96%E4%BD%99%EF%BC%8C%E4%B8%8D%E8%B6%B3448%E7%9A%84%E8%BF%9B%E8%A1%8C%E8%A1%A5%E4%BD%8D%EF%BC%8C%E5%A1%AB%E5%85%85%E7%9A%84%E6%96%B9%E6%B3%95%E6%98%AF%E7%AC%AC%E4%B8%80%E4%BD%8D%E8%A1%A51%EF%BC%8C%E5%85%B6%E4%BB%96%E4%BD%8D%E8%A1%A50%E3%80%82,%E7%BB%8F%E8%BF%87%E8%A1%A5%E4%BD%8D%E5%90%8E%EF%BC%8C%E7%8E%B0%E5%9C%A8%E7%9A%84%E9%95%BF%E5%BA%A6%E4%B8%BA512%2AN%2B448%E3%80%82-->
<!--
## <i class="fa-solid fa-beat-fade fa-microchip"></i> 03 算法大致过程
-->

<!--https://zhuanlan.zhihu.com/p/94619052-->
<!--https://www.cxymm.net/article/u011583927/80905740-->
<!--TODO:-->

> 其实hash 大致的基本过程都是一样的

* 补位

> 最终的长度是512位的倍数

* 分块

* 计算

> 常量初始化 -> 消息(数据/报文)预处理 -> 摘要计算主循环 -> 逻辑函数定义

<!--s-->
## <i class="fa-solid fa-video"></i> 04 video simulator
<!--add videos gifs-->

<!--s-->
# <i class="fa-solid fa-pen-to-square"></i> 体会破解难度

<!--v-->
* xxx

<!--v-->
> ps: 以上数据及相关图片均来自视频xxx,by 3blue1one(一个制作数学动画的视频频道， 致力与将数学活灵活现的展示出来)

<!--s-->
# <i class="fa-solid fa-image"></i> 05  图片

<!--v-->
<img src="images/sha256go.png" style="zoom: 50%;" />

<!--v-->
<img src="images/sha256result.png" style="zoom: 100%;" />

<!--v-->
<img src="images/sha256demo.png"  style="zoom: 50%;" />

<!--s-->
# <i class="fa-solid fa-car"></i> 资料

<!--v-->
> 维基百科： wikipedia.org
<!-- .element: class="fragment" -->
> 美国国家标准技术研究院相关文献(pdf) csrc.nist.gov/csrc/media/publications/fips/180/2/archive/2002-08-01/documents/fips180-2.pdf
<!-- .element: class="fragment" -->
> 论坛：mathmagic.cn
<!-- .element: class="fragment" -->
> https://www.securityweek.com/first-sha-1-collision-attack-conducted-google-cwi
<!-- .element: class="fragment" -->
> shattered.io
<!-- .element: class="fragment" -->
<!--s-->
##  <i class="fa-solid fa-spin fa-hourglass-end"></i> 结束

==> 本次演示内容定会有不妥和错误之处，请多指正

<!--
  https://revealdoc.vercel.app
-->

<!--v-->
<div style="text-align: right;display: inline;">
<img src="images/qr.png" title="qr" alt="qr" style="zoom: 100%;" />
</div>

<!--v-->



<style>
#bottom-left {
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    text-align: left;
}
#bottom-right {
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    text-align: right;
	font-size: 30px;
}
</style>
